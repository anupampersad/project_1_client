import './App.css';
import LogicLogin from './Components/LogicLogin';
import LogicSignup from './Components/LogicSignup';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import React, { Component } from 'react'

export default class App extends Component {
  render() {
    return (
      <div>
        <BrowserRouter>
          <Switch>
            <Route exact path="/" component={LogicLogin} />
            <Route exact path="/signup" component={LogicSignup} />
          </Switch>
        </BrowserRouter>
      </div>
    )
  }
}