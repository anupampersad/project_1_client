import React, { Component } from 'react';
import "./form.css";
import axios from 'axios';
import Signup from './Signup';

export default class LogicSignup extends Component {

    constructor(props) {

        super(props)

        this.state = {

            firstName: '',
            lastName: '',
            email: '',
            age: '',
            password: '',
            terms: '0',

            firstNameError: false,
            lastNameError: false,
            emailError: false,
            ageError: false,
            passwordError: false,
            termsError: false
        }
    }

    changeHandler = (event) => {
        this.setState({ [event.target.name]: event.target.value });
    }

    checkErrors = () => {

        let firstNameError, lastNameError, emailError, ageError, passwordError, termsError

        const firstNameFormat = /^[A-Za-z]{4,20}$/

        if (this.state.firstName.match(firstNameFormat)) {

            this.setState({ firstNameError: false })
            firstNameError = false
        }
        else {
            this.setState({ firstNameError: true })
            firstNameError = true
        }

        // ________________

        const lastNameFormat = /^[A-Za-z]{4,50}$/

        if (this.state.lastName.match(lastNameFormat)) {

            this.setState({ lastNameError: false })
            lastNameError = false
        }
        else {
            this.setState({ lastNameError: true })
            lastNameError = true
        }

        // ________________

        let mailformat = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,3}))$/;

        if (this.state.email.match(mailformat)) {

            this.setState({ emailError: false })
            emailError = false
        }
        else {
            this.setState({ emailError: true })
            emailError = true
        }

        // ________________

        if ((parseInt(this.state.age) < 4) || this.state.age.length == 0) {

            this.setState({ ageError: true })
            ageError = true
        }
        else {
            this.setState({ ageError: false })
            ageError = false
        }

        // ________________

        let passwordFormat = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,20}$/

        if (this.state.password.match(passwordFormat)) {

            this.setState({ passwordError: false })
            passwordError = true
        }
        else {
            this.setState({ passwordError: true })
            passwordError = false
        }

        // ________________

        if (this.state.terms == '0') {

            this.setState({ termsError: true })
            termsError = true
        }
        else {
            this.setState({ termsError: false })
            termsError = false
        }

        // final check 

        if (firstNameError  || lastNameError  || emailError  || ageError  || passwordError  || termsError ) {
            return true
        }
        else {
            return false
        }
    }

    submitHandler = async (event) => {

        const errors = this.checkErrors();

        if (errors) {
            event.preventDefault();
        }
        else {

            const data = {
                firstName: this.state.firstName,
                lastName: this.state.lastName,
                email: this.state.email,
                age: this.state.age,
                password: this.state.password
            }

            await axios.post('/users', data)
                .then((res) => {
                    console.log(res)
                })
                .catch((error) => {
                    console.log(error)
                    event.preventDefault();
                })
        }
    }

    render() {
        return (
            <div>
                <Signup
                    changeHandler={this.changeHandler}
                    submitHandler={this.submitHandler}
                    firstNameError={this.state.firstNameError}
                    lastNameError={this.state.lastNameError}
                    emailError={this.state.emailError}
                    ageError={this.state.ageError}
                    passwordError={this.state.passwordError}
                    termsError={this.state.termsError}
                />
            </div>
        )
    }
}
