import React, { Component } from 'react';
import "./form.css";
import axios from 'axios';
import Login from './Login';

export default class LogicLogin extends Component {

    constructor(props) {

        super(props)

        this.state = {

            email: '',
            password: '',

            emailError: false,
            passwordError: false,
        }
    }

    changeHandler = (event) => {
        this.setState({ [event.target.name]: event.target.value });
    }

    checkErrors = () => {

        let emailError, passwordError

        let mailformat = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,3}))$/;

        if (this.state.email.match(mailformat)) {

            this.setState({ emailError: false })
            emailError = false
        }
        else {
            this.setState({ emailError: true })
            emailError = true
        }

        // ________________

        let passwordFormat = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,20}$/

        if (this.state.password.match(passwordFormat)) {

            this.setState({ passwordError: false })
            passwordError = false
        }
        else {
            this.setState({ passwordError: true })
            passwordError = true
        }

        // final check 

        if (emailError || passwordError) {
            return true;
        }
        else {
            return false;
        }
    }

    submitHandler = async (event) => {

        const errors = this.checkErrors();

        if (errors){
            event.preventDefault();
        }
        else{

            const data = {
                email: this.state.email,
                password: this.state.password
            }

            await axios.post('/users/login', data)
            .then((res) => {
                console.log(res)
                console.log('inside then')
            })
            .catch((error) => {
                console.log(error)
                event.preventDefault();
            })
        }
    }

    render() {
        return (
            <div>
                <Login
                    changeHandler={this.changeHandler}
                    submitHandler={this.submitHandler}
                    emailError={this.state.emailError}
                    passwordError={this.state.passwordError}
                />
            </div>
        )
    }
}
