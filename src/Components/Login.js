import React, { Component } from 'react';
import "./form.css";

export default class Login extends Component {

    constructor(props) {
        super(props)
    }

    render() {

        const { changeHandler, submitHandler } = this.props;
        const { emailError, passwordError } = this.props;

        return (
            <div id="main-container">

                <div id="sign-up">
                    <h1>LOGIN</h1>
                    <form className='login-form' onSubmit={submitHandler} >

                        <div className="form-input email">
                            <label htmlFor="email-address">Email Address</label>
                            <input type="email" id="email" name="email" placeholder="Email address" onChange={changeHandler} />
                            {
                                emailError
                                    ? <span className="error-message invalid">Invalid email address</span>
                                    : <></>
                            }
                        </div>

                        <div className="form-input">
                            <label htmlFor="password">Password</label>
                            <input type="password" id="password" name="password" placeholder="Password" onChange={changeHandler} />
                            {
                                passwordError
                                    ? <span className="error-message invalid" style={{ textAlign: "center", gridColumn: 1 / 3 }}>
                                        Password must be 8-20 characters long, must have atleast 1 Uppercase Letter, 1 Lower case letter,
                                        1 Special character
                                    </span>
                                    : <></>
                            }
                        </div>

                        <div className="form-input">
                            <button id="submit-button" type="submit"> submit</button>
                        </div>
                    </form>

                    <div className="redirect">Not Registerd Yet? <a href="/signup">Signup</a></div>

                </div>
            </div>
        )
    }
}
