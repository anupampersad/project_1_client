import React, { Component } from 'react';
import "./form.css";

export default class Signup extends Component {

    constructor(props) {

        super(props)
    }

    render() {

        const { changeHandler, submitHandler} = this.props;
        const { firstNameError, lastNameError, emailError, ageError, passwordError, termsError} = this.props;
        
        return (
            <div id="main-container">

                <div id="sign-up">
                    <h1>Sign Up Form</h1>
                    <form className='sign-up-form' onSubmit={submitHandler}>

                        <div className="form-input">
                            <label htmlFor="fname">First Name</label>
                            <input type="text" id="fname" name="firstName" placeholder="First Name" onChange={changeHandler} />
                            {
                                firstNameError
                                    ? <span className="error-message invalid">That does not sound correct</span>
                                    : <></>
                            }
                        </div>

                        <div className="form-input">
                            <label htmlFor="lname">Last Name</label>
                            <input type="text" id="lname" name="lastName" placeholder="Last Name" onChange={changeHandler} />
                            {
                                lastNameError
                                    ? <span className="error-message invalid">That does not sound correct</span>
                                    : <></>
                            }

                        </div>

                        <div className="form-input email">
                            <label htmlFor="email-address">Email Address</label>
                            <input type="text" id="email" name="email" placeholder="Email address" onChange={changeHandler} />
                            {
                                emailError
                                    ? <span className="error-message invalid">Invalid email address</span>
                                    : <></>
                            }
                        </div>

                        <div className="form-input">
                            <label htmlFor="age">How young are you ?</label>
                            <input type="number" id="age" name="age" placeholder="age"
                                onKeyPress={(event) => { if (event.target.value.length == 2) return false }}
                                onChange={changeHandler} />
                            {
                                ageError
                                    ? <span className="error-message invalid">Invalid Age</span>
                                    : <></>
                            }
                        </div>

                        <div className="form-input">
                            <label htmlFor="password">Password</label>
                            <input type="password" id="password" name="password" placeholder="Password" onChange={changeHandler} />
                            {
                                passwordError
                                    ? <span className="error-message invalid" style={{ textAlign: "center", gridColumn: 1 / 3 }}>
                                        Password must be 8-20 characters long, must have atleast 1 Uppercase Letter, 1 Lower case letter,
                                        1 Special character
                                    </span>
                                    : <></>
                            }
                        </div>

                        <div className="form-input">
                            <div id="toc">
                                <label htmlFor="terms"> I agree to the <a href="">terms and condition</a> </label>
                                <input type="checkbox" id="terms" name="terms" value="1" onChange={changeHandler} />
                                
                                {
                                    termsError
                                        ? <span className="error-message invalid" style={{ textAlign: "center", gridColumn: 1 / 3 }}>Please agree to the
                                        terms and conditions</span>
                                        : <></>
                                }
                            </div>
                        </div>

                        <div className="form-input">
                            <button id="submit-button" type="submit"> submit</button>
                        </div>

                    </form>

                    <div className="redirect">Already a registered user? <a href="/">Login</a></div>

                </div>

            </div>
        )
    }
}
